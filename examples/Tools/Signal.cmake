if(BUILD_EXAMPLES)
	option(BUILD_SIGNAL_EXAMPLES "Should we build Signal examples" FALSE)
	if(BUILD_SIGNAL_EXAMPLES)
		add_subdirectory(Signal)
	endif()
endif()

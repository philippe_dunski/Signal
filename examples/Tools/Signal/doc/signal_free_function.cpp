/* soit la fonction libre */
void mySlot(){
    /* ... */
}
int main(){
    Signal sig;
    /* on connecte mySlot ici */
    sig(); // l'émission du signal invoque automatiquement mySlot()
       
}

class SignalHolder{
public:
    Signal <int, int > sig;
};
/* et la classe "réceptrice" */
class SlotHandler{
public: 
    void mySlot(int, int );
};
int main(){
    SlotHandler * slotHandler{new SlotHander};
    SignalHolder sigHolder;
    if(slotHandler){
		/* connectons slotHandler.mySlot au signal SignalHolder.sig */
    }
    /* Allez savoir ce que l'on fait ici */
     * ... */
     sigHolder.sig(3,5); // émet le signal. Invoque sigHander->mySlot
                         // en lui transmettant les valeurs 3 et 5
}

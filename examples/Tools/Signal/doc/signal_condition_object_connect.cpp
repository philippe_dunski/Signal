class Object{
public:
    void someMemberFunction();
};
int main(){
	/* déclarer un signal qui n'envoie aucune donnée */
    Signal sig;
    if(condition){
        Object obj;
        /* #1 connecter obj.someMemberFunction ici */
        sig();
    }          // #2 : CRACK
    sig();     // #3 : BOOM 
}

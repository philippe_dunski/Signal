
/* Soit la classe "réceptrice" */
class SlotHandler{
public: 
    void mySlot(int, int );
};
int main(){
    std::vector<SlotHandler> tab;
    /* créons des plusieurs récepteurs */
    for(size_t i=0; i<10; ++i){
        tab.push_back(SlotHander{});
    }
    
    Signal <int, int > sig; /* créons un signal correspondant */
    for(auto const & it : tab){
       /* connectons nos objets au signal ici */
    }
    /* Allez savoir ce que l'on fait ici */
     * ... */
    sig(3,5); // Emet le signal.  Invoque la fonction membre mySlot
              // sur tous les objets créés en leur transmettant les
              // valeur 3 et 5 
}

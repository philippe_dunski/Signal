/** @file call_order.cpp Tools/Signal/call_order.cpp
 * @brief Cet exemple démontre que l'ordre d'appel dépend de l'ordre de connection
 *
 */
void addition(int i, int j){
    std::cout<<"Addition  : "<<i<<" + "<<j<<" = "<< i + j<<"\n";
}
void Substraction(int i, int j){
    std::cout<<"Substraction  : "<<i<<" - "<<j<<" = "<< i - j<<"\n";
}
void Multiplication(int i, int j){
    std::cout<<"Multiplication  : "<<i<<" * "<<j<<" = "<< i * j<<"\n";
}
int main(){
    Signal<int, int> sig;
    auto conn1 = sig.connect(addition);
    auto conn2 = sig.connect(substraction);
    auto conn3 = sig.connect(multilication);
    sig(8, 3);
}

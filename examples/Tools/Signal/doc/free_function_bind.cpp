
void add(int & result, int i, int j){
    result = i + j;
}
class Multiply{
	static void execute(int & result, int i, int j){
		result = i * j;
	}
}
int main(){
    using namespace std::placeholders;
    /* un signal transmettant deux valeur de type int */
    Signal< int, int> sig;
    int total;
    int product;
    /** permettre à la fonction add de servir de slot en lui transmettant
     *  total comme référence pour le premier paramètres
     */
    auto f1 = std::bind(add, std::ref(total),_1,_2);
    /** permettre à la fonction Multipyll::execute de servir de slot en
     *  lui transmettant total comme référence pour le premier paramètres
     */
    auto f2 = std::bind(Multiply::execute, std::ref(product),_1,_2);
    /* connecter f1 et f2 au signal ici */
    sig(2,3); // 1- invoque f1 (la fonction add) en lui transmettant les valeurs
              //    2 et 3
              // 2- invoque f2 (Multiply::execute) en lui transmettant les valeurs
              //    2 et 3
    /* afficher les résultats */
    std::cout<< total<<"\n";
    std::cout<< product<<"\n";
}

class Object{
public:
    void someMemberFunction();
};

int main(){
   Object obj;
   if(/* condition */){
	   /* déclarer un signal qui n'envoie aucune donnée
       Signal sig;
       /* connecter obj.someMemberFunction ici */
       sig(); // invoque obj.someMemberFunction()
   } // sig est détruit ici
   /* au pire, obj.someMemberFunction ne sera plus jamais invoqué ici */
}

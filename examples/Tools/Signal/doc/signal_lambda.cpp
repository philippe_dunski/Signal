int main(){
    int result;
    /* déclarer une expression lambda qui capture le contexte ainsi
     * que deux entier sous forme de paramètres
     */
    auto foo = [&](int i, int j){
        result = i + j;
    };
    /* déclarer un signal qui envoie deux entier comme paramètres
    Signal <int, int> sig;
    /* connecter l'expression lambda au signal ici */
    sig(3, 4); /* invoque l'expression lambda en lui transmettant les
                * valeurs 3 et 4 comme paramètres
                */ 
}

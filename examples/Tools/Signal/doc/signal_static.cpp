/* Soit une classe */
class MyClass{
public:
    static void mySlot();
};
int main(){
    Signal sig;
    /* on connecte la fonciton MyClass::mySlot au signal ici */
    sig(); // l'émission du signal invoque automatiquement MyClass::mySlot()
}

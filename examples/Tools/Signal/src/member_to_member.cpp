/** @example member_to_member.cpp
  *
  * Cet exemple démontre la facilité avec laquelle il est possible
  * de connecter un signal utilisé en interne dans une classe
  * au slot exposé sous la forme d'une fonction membre @b non @b statique
  * appartenant à une autre
  *
  */
#include <Signal.hpp>
#include <iostream>
/* la classe qui fournit le signal */
class SigHolder{
    /* un alias de type sur le signal utilisé, pour la facilité d'écriture*/
    using sig_t = Tools::Signal<int>;
public:
    /* un alias de type sur le genre de slots qui pourront se connecter
     * au signal, pour la facilité d'écriture
     */
    using slot_t = typename sig_t::slot_type;
    /* permet de connecter un slot au signal */
    Tools::Connection connect(slot_t slot){
        return  sig_.connect(slot);
    }
    /* permet d'émettre le signal, en lui fournissant le parametre attendu*/
    void transmit(int i){
        sig_(i);
    }
private:
    sig_t sig_;
};
/* la classe qui fournit le slot devant réagir au signal */
class SlotHolder{
public:
    /* La fonction membre non statique qui servira de slot */
    void signalReceived(int i){
        total_+=i;
        std::cout<<"Got signal with value "<<i<<"\n"
                 <<"New internal value is "<<total_<<"\n";
    }
    /* La fonction permettant d'effectivement connecter le signal au slot*/
    void onSignalReceived(SigHolder & emitter){
        /* il est possible d'utiliser les expressions lambda et de profiter
         * de la capture
         */
        connection_ = emitter.connect([&](int i){signalReceived(i);});
    }
private:
    Tools::Connection connection_;
    int total_{15};

};
int main(){
    /* l'ordre dans lequel on crée les deux objets n'a aucune importance,
     * vu qu'il faut disposer des deux pour pouvoir les connecter;
     */
    SigHolder emitter;
    SlotHolder receptor;
    receptor.onSignalReceived(emitter);
    /* Faisons émettre le signal avec différentes valeurs */
    emitter.transmit(2);
    emitter.transmit(5);
    emitter.transmit(10);
}

/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @addtogroup SignalExamples
 * @{
 * @defgroup ConnectLambda Exemple simple d'utilisation d'une "expression lambda" comme slot 
 * @}
 * @}
 * @}
 */
 
/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @ingroup SignalExamples
 * @{
 * @addtogroup ConnectLambda
 * @{
 */
/** @file  Tools/Signal/src/connect_lambda.cpp connect_lambda.cpp
 * 
  *
  * @author Philippe Dunski <dunski.philippe@gmail.com>
  * @version 1.0
  * @todo have a better dealing with threads
  * @copyright MIT License
  * 
  * @brief exemple d'utilisation d'une expression lambda comme slot
  * 
  * Depuis l'arrivée de C++11, nous disposons de la possibilité d'utiliser
 * ce qu'il convient d'appeler des @b expressions @b lambda.
 *
 * Il s'agit, pour faire simple, d'une alternative à l'utilisation des
 *  @em foncteurs lorsqu'il apparait de tout évidence qu'il serait
 * inefficace de le créer pour n'avoir l'occasion que de l'utiliser une
 * seule fois.
 *
 * Dans ce genre de situation, il est alors sans doute préférable de créer
 * ce que nous pourrions comparer à des "fonctions locales".
 *
 * Il est tout à fait possible d'utiliser les expressions lambda pour
 * définir un slot que l'on souhaite connecter au signal:
 *
 * Après avoir défini le signal qui sera utilisé, il "suffira" de transmettre
 * l'expression lambda qui nous intéresse à la fonction @fn connect du
 * signal en question, sans oublier bien sur de récupérer l'objet de type
 * Tools::Connexion qui sera renvoyé par cette fonction
 *
 */ 
// Copyright 2018 Philippe Dunski <dunski.philippe@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy 
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in 
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <Signal.hpp>
#include <iostream>
int main(){
    int localValue{0};
    /* prints "Starting value 0" */
    std::cout<<"Starting value "<<localValue<<"\n";
    Tools::Signal<int> sig;
    auto conn = sig.connect([&](int i){localValue+=i;});
    sig(5);// calls the lambda expression with 5 as parameter
    std::cout<<"Final value "<<localValue<<"\n";
}

/**@}
 * @}
 * @}
 * @}*/

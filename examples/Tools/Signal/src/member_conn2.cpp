/** @example member_conn2.cpp
 *
 * Cet exemple démontre comment utiliser le signal transmis en paramètre
 * au constructeur d'une classe pour initialiser correctement une
 * connexion déclarée comme donnée membre d'un classe directement dans
 * la liste d'initialisation
 *
 * @note Cet exemple utilise @c std::bind, mais l'utilisation d'une
 * @em expression @em lambda reste toujours envisageable et peut être
 * considérée par beaucoup comme une alternative bien plus facile à
 * mettre en oeuvre
 *
 */
#include <Signal.hpp>
#include <functional>
#include <iostream>
/* Un alias de type représentant un signal qui transmet une (unique) valeur
 * numérique aux slots auxquels il est connecté
 *
 * L'utilisation de tels alias de type tend à  faciliter grandement l'écriture
 * (et la compréhension) du code
 */
using IntSignal = Tools::Signal<int>;
/* La structure qui fournira le slot qui doit se connecter au signal
 *
 */
struct MyStruct{
    /* Le constructeur de la structure.  Il prend une valeur numérique
     * destinée à initialiser la donnée membre value comme paramètre en
     * plus du signal auquel il faudra connecter le slot
     */
    MyStruct(int v, IntSignal & sig):
        value{v},
        conn{sig.connect(std::bind(&MyStruct::addValue,
                                   std::ref(*this),
                                   std::placeholders::_1))}{
    }
    int value;
    void addValue(int i){
        value+=i;
    }
    Tools::Connection conn;
};
int main(){
    /* Le signal qui sera utilisé */
    Tools::Signal<int> sig;
    /* Créons l'instance de la structure qui sera manipulée */
    MyStruct a{5, sig};
    std::cout<<"Starting a value "<<a.value<<"\n";
    sig(10); // Emettons le signal
    //et affichons le résultat
    std::cout<<"Final a value "<<a.value<<"\n";

}

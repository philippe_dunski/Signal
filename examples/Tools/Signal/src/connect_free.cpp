/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @addtogroup SignalExamples
 * @{
 * @defgroup ConnectFree Un exemple simple de connexion d'une fonction libre
 * @}
 * @}
 * @}
 */
/** @example connect_free.cpp
 * Cet exemple démontre la facilité avec laquelle il est possible de
 * connecter une fonction libre à un signal.
 *
 * A partir du moment où l'on dispose du prototype d'une fonction qui s
 * s'accorde avec le signal que l'on prévoit d'émettre, il suffit
 * d'invoquer la foncton @function connect de la classe signal en fournissant
 * le nom de la fonction qui servira de slot.
 *
 * Bien sur, pour que le lien entre le signal et le slot puisse exister,
 * il est @b indispensable de récupérer l'objet de type Tools::Connexion
 * renvoyé par cette fonction
 *
 */
#include <Signal.hpp>
#include <iostream>
/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @ingroup SignalExamples
 * @{
 * @addtogroup ConnectFree
 * @{
 */
int externValue{0};
void addToExternValue(int i);
int main(){
    Tools::Signal<int> sig;
    auto conn = sig.connect(addToExternValue);
    /* prints "Starting value 0" */
    std::cout<<"Starting value "<<externValue<<"\n";
    sig(5); // calls addValue with 5 as parameter
    std::cout<<"Final value "<<externValue<<"\n";

}

void addToExternValue(int i){
    std::cout<<"Adding "<<i<<" to extern value\n";
    externValue+=i;
}

/**@}
 * @}
 * @}
 * @}*/

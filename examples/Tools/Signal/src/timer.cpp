/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @addtogroup SignalExamples
 * @{
 * @defgroup Timer Exemple simple de création d'un chronomètre
 * @}
 * @}
 * @}
 */
 
/** @ingroup Tools
/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @ingroup SignalExamples
 * @{
 * @addtogroup Timer
 * @{
 */
/** @file  Tools/Signal/src/timer.cpp timer.cpp
 * 
 *
 * @author Philippe Dunski <dunski.philippe@gmail.com>
 * @version 1.0
 * @todo have a better dealing with threads
 * @copyright MIT License
 * 
 * @brief exemple simple de création d'un chronomètre simple
 * 
 * Il est particulièrement simple de créer sa propre classe de chronomètre,
 * car il "suffit" de créer une classe qui sera en mesure d'émettre un signal
 * à intervales réguliers.
 * 
 * Dans cet exemple, je démontre la possibilité de transmettre directement
 * le timer au slot concerné ( gotSignal(Timer & t) ) tout en ajoutant
 * la possibilité de le modifier pour obtenir une pause le temps de voir un
 * temps intermédiaire et d'arrêter le crhonomètre après un temps clairement
 * défini.
 * 
 * D'autres solutions pourraient être envisagées
 * @note En théorie, un chronomètre pourrait fonctionner "jusqu'à la nuit des temps",
 * ce qui est confirmé sur mon système par la valeur renvoyée par la fonction max
 * de std::chrono::second (elle correspond à plus de 292 milliards d'années
 * sur mon système)
 * 
 * @note Cependant, l'expérience nous démontre que nous voudrons toujours
 * arrêter un chronomètre à un moment ou à un autre: parfois pour obtenir 
 * un temps intermédiaire, parfois de manière définitive car il n'est simplement
 * plus utile.
 * 
 * 
 */ 
 
// Copyright 2018 Philippe Dunski <dunski.philippe@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy 
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in 
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <Signal.hpp>
#include <chrono>
#include <thread>
#include <iostream>
#include <type_traits>
using Interval =  std::chrono::milliseconds;
using Second_t =   std::chrono::seconds;
class Timer{
	using clock_t = std::chrono::system_clock;
    using sig_t = Tools::Signal< >;
public:
    using slot_t = typename sig_t::slot_type;
    void execute(){
		startTime_ = clock_t::now();
        while(!stop_){
			std::this_thread::sleep_for(Interval{100});
			sig_();
        }
	}
    auto onTick(slot_t s){
        return sig_.connect(s);
    }
    void intermediate(){
		intermediate_ = !intermediate_;
	}
	bool inItermediaireMode() const{
		return intermediate_;
	}
	bool finished() const{
		return stop_;
	};
	void stop(){
		stop_=true;
	}
	Interval  enlapsedTime()const{
		clock_t::time_point temp{clock_t::now() };
		return std::chrono::duration_cast<Interval>(temp - startTime_);
	}
private:
    sig_t sig_;
    clock_t::time_point startTime_{clock_t::now()};
    bool stop_{false};
    bool intermediate_{false};
};
void stopTimer(Timer & t){
		t.stop();
}

void gotSignal(Timer & t){
	if(std::chrono::duration_cast<Second_t>(t.enlapsedTime()).count() >10 )
		stopTimer(t);
	if(!t.inItermediaireMode() && !t.finished())
		std::cout<<"Got signal after "<<t.enlapsedTime().count()<< " miliseconds\n";
}
int main(){
	std::cout<<Interval::max().count()<<"\n";
    Timer t;
    auto conn = t.onTick([&](){gotSignal(t);});
    t.execute();
    std::cout<<"Timer fihished\n";

}

/**@}
 * @}
 * @}
 * @}*/

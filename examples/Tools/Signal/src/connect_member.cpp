/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @addtogroup SignalExamples
 * @{
 * @defgroup ConnectMember Example simple d'utilisation de fonctions membres (non statiques) comme slot
 * @}
 * @}
 * @}
 */
 
/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @ingroup SignalExamples
 * @{
 * @addtogroup ConnectMember
 * @{
 */
/** @file  Tools/Signal/src/member_connect.cpp member_connect.cpp
 * 
 *
 * @author Philippe Dunski <dunski.philippe@gmail.com>
 * @version 1.0
 * @todo have a better dealing with threads
 * @copyright MIT License
 * 
 * @brief exemple d'utilisation d'une expression lambda comme slot
 * 
 * Cet exemple explique comment connecter n'importe quelle fonction
 * membre @b non @b statique d'une classe ou d'une structure en utilisant
 * @c std::bind et les @em placeholders.
 *
 * @note Bien qu'il soit toujours utile et intéressant de pouvoir
 * connecter un slot à un signal de cette manière, l'utilisation
 * d'une @b expression @b lambda est souvent jugée bien plus simple.  Je
 * vous en laisse seul juge.
 *
 * @sa @ref connect_lambda2.cpp
 *
 */
// Copyright 2018 Philippe Dunski <dunski.philippe@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy 
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in 
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <Signal.hpp>
#include <functional>
#include <iostream>

struct MyStruct{
    MyStruct(int v):value{v}{
    }
    int value;
    void addValue(int i){
        value+=i;
    }
};
int main(){
    using namespace std::placeholders;
    MyStruct a{0};
    MyStruct b{10};
    /* prints "Starting a value 0" */
    std::cout<<"Starting a value "<<a.value<<"\n";
    /* prints "Starting b value 10" */
    std::cout<<"Starting b value "<<b.value<<"\n";
    Tools::Signal<int> sig;
    auto conn1=sig.connect(std::bind(&MyStruct::addValue,std::ref(a),_1 ));
    auto conn2=sig.connect(std::bind(&MyStruct::addValue,std::ref(b),_1 ));
    sig(3);
    std::cout<<"After signal emission a.value's value is "<<a.value<<"\n"
             <<"After signal emission b.value's value is "<<b.value<<"\n";
}

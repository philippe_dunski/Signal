/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @addtogroup SignalExamples
 * @{
 * @defgroup ConnectLambda2 Un autre exemple simple d'utilisation d'une "expression lambda" comme slot 
 * @}
 * @}
 * @}
 */
 
/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @ingroup SignalExamples
 * @{
 * @addtogroup ConnectLambda2
 * @{
 */


/** @file  Tools/Signal/src/connect_lambda2.cpp connect_lambda2.cpp
 * 
 * @author Philippe Dunski <dunski.philippe@gmail.com>
 * @version 1.0
 * @todo have a better dealing with threads
 * @copyright MIT License
 * 
 * @brief un autre exemple d'utilisation des expressions lambda
 * 
 * L'utilisation de @c std::bind afin d'utiliser des fonctions membres
 * @b non @b statiques de classes ou de structure comme slot n'est parfois
 * pas aussi "instinctive" qu'elle ne pourrait le sembler, car il faut
 * à la fois veiller à transmettre l'objet courant à partir duquel faire
 * appel à la fonction servantde slot et mettre en place les @em placeholders
 * permettant de "capturer" les paramètres qui devront être transmis au
 * slots.
 *
 * Du fait de leur capacité à capturer le contexte dans lequel elles sont
 * définies, les expressions lambda rendent cette manoeuvre beaucoup plus
 * facile à effectuer, car il s'agit désormais d'un code proche de
 *
 */
// Copyright 2018 Philippe Dunski <dunski.philippe@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy 
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in 
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <Signal.hpp>
#include <iostream>
/* soit une structure qui expose une fonction prenant deux valeurs entières
 * en paramètres et ayant une forme générale proche de
 */
struct MyStruct{
    int value;
    void addValue(int i, int j){
        value = i + j;
    }
};
int main(){
    /* Nous pouvons définir un signal qui transmettra deux valeurs entières
     * comme paramètre sous la forme de
     */
    Tools::Signal<int, int> sig;
    /* nous pouvons créer une instance de la strcture de manière tout
     * à fait classique
     */
    MyStruct obj;
    /* Et utiliser les expression lambda pour connecter le fonction
     * void MyStruct::addValue au signal sous la forme d'une expression
     * lambda
     */
    auto conn = sig.connect([&]                // capture le contexte sous forme
                                               // de référence
                            (int i, int j){    // les paramètres qui seront transmis
                            obj.addValue(i,j); // le corps de l'expression lanbda
                            });                // qui invoque la fonction membre
                                               // non statique en interne
    sig(4, 5);
    std::cout<<"après emission du signal, obj.value vaut "<<obj.value<<"\n";
}

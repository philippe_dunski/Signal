/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @addtogroup SignalExamples
 * @{
 * @defgroup CountDown Un exemple simple de compte à rebours
 * @}
 * @}
 * @}
 */

/** @example CountDown.cpp
 * @brief Cet exemple propose une manière simple d'obtenir un compte à rebours simple
 *
 * Pour pouvoir disposer d'un compte à rebours simple, nous devons pouvoir
 * émettre deux type de signaux :
 * 
 * - Le premier sera émis à intervales réguliers aussi longtemps que
 * le temps imparti ne sera pas terminé;
 * - le second sera émis spécifiquement lorsque le temps imparti sera
 * terminé
 * 
 * Le signal émis à intervals réguliers devras en outre transmettre l'information
 * du temps restant avant l'échéance, alors que celui qui sera émis à l'échéance
 * pourra ne transmettre aucune information particulière
 * 
 * Le plus facile est donc de créer un alias de type pour chacun de ces
 * signaux, et d'exposer, en plus, un alias de type pour le slot spécifique
 * à chacun de ces signaux dans l'accessibilité publique.
 */
#include <Signal.hpp>
#include <chrono>
#include <thread>
#include <iostream>
#include <type_traits>
/** @ingroup Tools
 * @{
 * @ingroup ToolsExamples
 * @{
 * @ingroup SignalExamples
 * @{
 * @addtogroup CountDown
 * @{
 */
/** @brief alias de type représentant l'interval utilisé
 * 
 */
using Interval =  std::chrono::seconds;
/** @brief représente la notion de compte à rebours
 * 
 */
class CountDown{
	/** @brief alias de type sur le signal utilisé à interval réguilier
	 * 
	 * @note Cet alias de type peut être placé dans l'accessibilité privée
	 * car il ne sera utilisé que pour définir la donnée membre
	 */
    using tickSig_t = Tools::Signal<Interval>;
	/** @brief alias de type sur le signal utilisé à échéance du compte à rebours
	 * 
	 * @note Cet alias de type peut être placé dans l'accessibilité privée
	 * car il ne sera utilisé que pour définir la donnée membre
	 */
    using sigFinished_t  = Tools::Signal<>;
public:
	/** @brief alias de type sur le slot à utiliser à interval régulier
	 * 
	 * @note Cet alias de type est défini dans l'accessibilité publique
	 * car il doit être accessible au travers de la fonction onTick
	 */
    using tickSlot_t       = typename tickSig_t::slot_type;
	/** @brief alias de type sur le slot à utiliser à échéance du compte à rebours
	 * 
	 * @note Cet alias de type est défini dans l'accessibilité publique
	 * car il doit être accessible au travers de la fonction onFinished
	 */
    using slotFinished_t  = typename sigFinished_t ::slot_type;
    /** @brief permet la connexion d'un slot au signal émis à intervale réguier
     * 
     * @param[in] s slot à connecter
     * @return notion de connexion représentant le lien entre le signal et le slot
     * 
     */
    auto onTick(tickSlot_t s){
        return tick_.connect(s);
    }
    /** @brief permet la connexion d'un slot au signal émis à échéance du compte à rebours
     * 
     * @param[in] s slot à connecter
     * @return notion de connexion représentant le lien entre le signal et le slot
     * 
     */
    auto onFinished(slotFinished_t s){
        return finished_.connect(s);
    }
    /** @brief Lance le compte à rebours
     * 
     * @param[in] max temps maximal imparti pour le compte à rebours
     * 
     */
    void run(Interval max){
        while(max > Interval{}){
            std::this_thread::sleep_for(Interval{1});
            max -= Interval{1};
            tick_(max);
        }
        finished_();
    }
private:
    tickSig_t tick_;
    sigFinished_t finished_;
};
/** @brief fonction (libre dans le cas présent) utilisée comme slot pour le signal émis à intervale régulier
 * 
 * @param[in] time Temps restant avant échéance du compte à rebours
 * 
 */
void waiting(Interval time){
    std::cout<<"Please wait for "<< time.count()<<" seconds \n";
}

/** @brief fonction (libre dans le cas présent) utilisée comme slot pour le signal émis à échéance du compte à rebours
 * 
 */
void finished(){
    std::cout<<"Time out.  Thank you\n";
}
int main(){
    CountDown ct;
    auto conn1=ct.onTick(waiting);
    auto conn2=ct.onFinished(finished);
    ct.run(Interval{15});
    return 0;

}

/**@}
 * @}
 * @}
 * @}*/
